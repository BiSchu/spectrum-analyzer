#!/usr/bin/env python

import os
import sys
from datetime import datetime
import time


import shutil
import config

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from optparse import OptionParser
import osmosdr
from gnuradio import filter
from scipy import signal

import h5py
import numpy
import matplotlib.pyplot as plt

class IapSpectrumWriter(object):

    def __init__(self,cfg_file):#,duration):
        
        ############################################################## read Config
        self.__cfg = config.Config(cfg_file)
        self.n_int=self.__cfg["Parameters"]["FourierMean"]
        self.n_fft=self.__cfg["Parameters"]["FourierSize"]
        self.n_chan=len(self.__cfg["Devices"])
        self.f_mid=self.__cfg["Parameters"]["FreqMid"]
        self.f_sample=self.__cfg["Parameters"]["SampleRate"]
        dir = self.__cfg["Parameters"]["Directory"]
        self.__t_meas = self.__cfg["Parameters"]["TimeInterval"]
        self.__t_newHDF = self.__cfg["Parameters"]["TimePerHDF"]
        self.__address = self.__cfg["Devices"]["Address"]
        self.__rf_gain = self.__cfg["Devices"]["RfGain"]
        self.__if_gain = self.__cfg["Devices"]["IfGain"]
        self.__bb_gain = self.__cfg["Devices"]["BbGain"]

        ############################################################## read Config

        ############################################################## Time
        t_now = int(numpy.ceil(time.time()))
        t_start  = t_now
        t_start -= t_now%self.__t_newHDF
        t_start += self.__t_newHDF
        t_diff   = t_start-t_now
        if t_diff <=5.0:
            t_start += self.__t_newHDF
        self.__t_start = t_start
        s_start = datetime.fromtimestamp(self.__t_start)
        s_start = s_start.strftime('%Y-%m-%dT%H:%M:%S')
        self.__s_start = s_start
        #self.__t_stop = self.__t_start+duration
        ############################################################## Time

        ############################################################## Directories and Filenames
        self.filename = ["/%s.bin"%self.__s_start,"/%s.hdf"%self.__s_start] 
        dir += "/%s[Hz]"%self.f_mid                                       
        dirs = [dir+"/bin",dir+"/hdf"]
        [os.system("mkdir -p %s"%dirs[i])for i in xrange(2)]
        self.__dirs = dirs
        files=[]
        for i in xrange(2):
            files.append(self.__dirs[i]+ self.filename[i])
        self.__files = files
        #hdf = h5py.File(self.__files[1],'w')
        #shutil.copy(cfg_file,dirs[0])
        ############################################################## Directories and Filenames


        self.taps_bmh = taps_bmh = signal.firwin(101, cutoff = self.f_sample/4-1, window = 'blackmanharris', pass_zero = True,fs=self.f_sample/2)
        self.linfreq=numpy.linspace(
            start=(self.f_mid-self.f_sample/4)/1000000,
            stop=(self.f_mid+self.f_sample/4)/1000000,
            num=self.n_fft
            )
        #hdf_freq = hdf.create_dataset("info/freq",(4096,),dtype='f',data=self.linfreq)
        #hdf_freq.attrs['description'] = 'fft frequencies / MHz'
        
        
        ################################################################################ loop
        while True:#time.time()<=self.__t_stop:
            t_now = int(numpy.ceil(time.time()))
            if t_now>=self.__t_start:
                ########################################################## Time and Files
                s_start = datetime.fromtimestamp(self.__t_start)
                s_start = s_start.strftime('%Y-%m-%dT%H:%M:%S')
                self.__s_start = s_start

                self.filename = ["/%s.bin"%self.__s_start,"/%s.hdf"%self.__s_start]
                files=[]
                for i in xrange(2):
                    files.append(self.__dirs[i]+ self.filename[i])
                self.__files = files
                ########################################################## Time and Files
                

                ########################################################## Gnuradio
                self.flowgraph=gr.top_block()
                ################################################ Blocks
                self.osmosdr_source_0 = osmosdr.source( args="numchan=" + str(1) + " " + "numchan=1 hackrf="+str(self.__address) )
                self.osmosdr_source_0.set_sample_rate(self.f_sample)
                self.osmosdr_source_0.set_freq_corr(0, 0)
                self.osmosdr_source_0.set_dc_offset_mode(0, 0)
                self.osmosdr_source_0.set_iq_balance_mode(0, 0)
                self.osmosdr_source_0.set_gain_mode(False, 0)
                self.osmosdr_source_0.set_gain(self.__rf_gain, 0)
                self.osmosdr_source_0.set_if_gain(self.__if_gain, 0)
                self.osmosdr_source_0.set_bb_gain(self.__bb_gain, 0)
                self.osmosdr_source_0.set_antenna('', 0)
                self.osmosdr_source_0.set_bandwidth(self.f_sample/2, 0)
                self.osmosdr_source_0.set_center_freq(self.f_mid, 0) #f_mid#
                self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_float*1,str(self.__files[0]), False) #Filesink# [0] == /bin
                self.blocks_file_sink_0.set_unbuffered(False)
                self.fft_vxx_0 = fft.fft_vcc(self.n_fft, True, (window.blackmanharris(self.n_fft)), True, 1)
                self.blocks_vector_to_stream_0 = blocks.vector_to_stream(gr.sizeof_float*1, self.n_fft)
                self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, self.n_fft)
                self.blocks_nlog10_ff_0 = blocks.nlog10_ff(10, self.n_fft, 0)
                self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vff(([1.0/float(self.n_int)]*self.n_fft))
                self.blocks_integrate_xx_0 = blocks.integrate_ff(self.n_int, self.n_fft)
                self.blocks_head_0 = blocks.head(gr.sizeof_float*1, self.n_fft)
                self.blocks_complex_to_mag_squared_0 = blocks.complex_to_mag_squared(self.n_fft)
                self.fir_filter_xxx_0 =  filter.fir_filter_ccf(2, (numpy.float64(taps_bmh/numpy.sum(taps_bmh)).tolist())) #filter#
                self.fir_filter_xxx_0.declare_sample_delay(0)
                ############################################### Connections
                self.flowgraph.connect((self.osmosdr_source_0, 0), (self.fir_filter_xxx_0, 0))
                self.flowgraph.connect((self.fir_filter_xxx_0, 0), (self.blocks_stream_to_vector_0, 0))
                self.flowgraph.connect((self.blocks_stream_to_vector_0, 0), (self.fft_vxx_0, 0))
                self.flowgraph.connect((self.fft_vxx_0, 0), (self.blocks_complex_to_mag_squared_0, 0))
                self.flowgraph.connect((self.blocks_complex_to_mag_squared_0, 0), (self.blocks_integrate_xx_0, 0))
                self.flowgraph.connect((self.blocks_integrate_xx_0, 0), (self.blocks_multiply_const_vxx_0, 0))
                self.flowgraph.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_nlog10_ff_0, 0))
                self.flowgraph.connect((self.blocks_nlog10_ff_0, 0), (self.blocks_vector_to_stream_0, 0))
                self.flowgraph.connect((self.blocks_vector_to_stream_0, 0), (self.blocks_head_0, 0))
                self.flowgraph.connect((self.blocks_head_0, 0), (self.blocks_file_sink_0, 0))
                ############################################### Run and close
                self.flowgraph.start()
                self.flowgraph.wait()
                #del self.blocks_file_sink_0
                del self.flowgraph
                del self.osmosdr_source_0
                ########################################################## Gnuradio

                ########################################################## Hdf5
                if self.__t_start%self.__t_newHDF != 0:
                    read_data = numpy.asarray(numpy.fromfile(self.__files[0],numpy.float32))
                    hdf_spectrum = hdf.create_dataset("spectra/s"+self.__s_start,(4096,),dtype='f',data=read_data)
                    hdf_time = hdf.create_dataset("timestamps/t"+self.__s_start,(1,),dtype='i',data=self.__t_start)
                else:
                    hdf = h5py.File(self.__files[1],'w')
                    hdf_freq = hdf.create_dataset("info/freq",(4096,),dtype='f',data=self.linfreq)
                    hdf_freq.attrs['description'] = 'fft frequencies / MHz'
                    read_data = numpy.asarray(numpy.fromfile(self.__files[0],numpy.float32))
                    hdf_spectrum = hdf.create_dataset("spectra/s"+self.__s_start,(4096,),dtype='f',data=read_data)
                    hdf_time = hdf.create_dataset("timestamps/t"+self.__s_start,(1,),dtype='i',data=self.__t_start)
                    hdf["spectra"].attrs['description'] = 'signal power / dB'
                    hdf["timestamps"].attrs['description'] = 'time of measurement / datetime'
                ########################################################## Hdf5

                self.__t_start+=self.__t_meas
            else:
                time.sleep(1)
        ################################################################################ loop
        


if __name__ == "__main__":
	
    #the second parameter gives the duration in seconds. Therefore measuring a full day would require putting in 86400
    writer = IapSpectrumWriter("%s/.spectrum-analyzer/cfg/iap-spectrum-analyzer.cfg"%os.getenv('HOME'))#,500)


    sys.exit()