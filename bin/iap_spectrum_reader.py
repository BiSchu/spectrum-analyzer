#!/usr/bin/env Python

import os
import sys
import config
import time

import h5py
import numpy
import matplotlib.pyplot as plt

from datetime import datetime
from datetime import timedelta

class IapSpectrumReader(object):

    def __init__(self,hdf_file):
        hdf = h5py.File(hdf_file,'r')

        hdf_power = []
        hdf_timestamps = []

        s_dsets=[]
        t_dsets=[]
        for dset in hdf["spectra"]:
            s_dsets.append(dset)
        for dset in hdf["timestamps"]:
            t_dsets.append(dset)
        s_dsets=numpy.asarray(s_dsets)
        t_dsets=numpy.asarray(t_dsets)

        [hdf_power.append(hdf["spectra"][dset]) for dset in s_dsets]

        [hdf_timestamps.append(hdf["timestamps"][dset]) for dset in t_dsets]

        hdf_freq = hdf["info"]["freq"]

        power = numpy.asarray(hdf_power)
        print "spectra finished"
        timestamps = numpy.asarray(hdf_timestamps)
        print "timestamps finished"
        linfreq = numpy.asarray(hdf_freq)
        
        curve=[]

        for i in xrange(power.shape[0]):
            curve.append(numpy.max(power[i,1024:1536]))


        dtime=[]
        [dtime.append(datetime.fromtimestamp(timestamp)) for timestamp in timestamps]
        duration = timedelta.total_seconds(dtime[-1]-dtime[0])

        fig=plt.figure()
        mesh=plt.pcolormesh(linfreq,dtime,power)
        c_bar=fig.colorbar(mesh)
        c_bar.set_label(hdf["spectra"].attrs["description"])
        plt.title("Spectrum around 34 MHz over %d seconds" % duration)
        plt.ylabel(hdf["timestamps"].attrs["description"])
        plt.xlabel(hdf["info"]["freq"].attrs["description"])
        
        figcurve=plt.figure()

        plt.plot(dtime,curve)
        plt.title("Activity at 32.55MHz over %d seconds" % duration)
        plt.ylabel("Received signal power / dB")
        plt.xlabel("Datetime")
        plt.grid()


        plt.show()


if __name__ == "__main__":

    cfg = config.Config("%s/.spectrum-analyzer/cfg/iap-spectrum-analyzer.cfg"%os.getenv('HOME'))
    f_mid=cfg["Parameters"]["FreqMid"]
    t_newHDF = cfg["Parameters"]["TimePerHDF"]
    dir = cfg["Parameters"]["Directory"]
    dir += "/%s[Hz]"%f_mid                                       
    dirs = [dir+"/bin",dir+"/hdf"]
    if len(sys.argv) > 1:
        s_target = sys.argv[1]
    else:
        #path = "/home/radar/Dokumente/iap-spectrum-analyzer/data/55000000.0[Hz]/2020-10-21T12:24:30.hdf"
        t_now = int(numpy.ceil(time.time()))
        t_latest=t_now
        t_latest -= t_now%t_newHDF
        # t_latest -= t_newHDF
        s_target = datetime.fromtimestamp(t_latest)
        s_target = s_target.strftime('%Y-%m-%dT%H:%M:%S')

    path = dirs[1]+"/"+s_target+".hdf"
    reader = IapSpectrumReader(path)

    sys.exit()