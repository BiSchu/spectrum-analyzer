[General information]
This Repository contains simple Software which allows you to analyze a radiospectrum over an extended time interval using the HackRF One.
The measurements are saved to .bin and .hdf files.

[Installation]
The Software was designed using a list of prerequisites.
Please find them together with installation commands below:
Ubuntu 18.04.5 LTS
Gnuradio 3.7.11 
    $ sudo apt install gnuradio
gr-osmosdr v0.1.5
    $ git clone https://github.com/osmocom/gr-osmosdr
    $ git checkout v0.1.5
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ sudo make install
    $ sudo ldconfig
hackrf
    $ sudo apt install hackrf
libhackrf-dev
    $ sudo apt-get install -y libhackrf-dev
python
    $ sudo apt install python
pyton-pip: 
	$ pip install config
	$ pip install scipy
	$ pip install h5py
	$ pip install docopt
Depending on previous use of the System you might be prompted about missing packages such as git, swig, cmake, python-pip. Install those through apt.

How to install:
-Clone the repository

    $ git clone https://gitlab.com/BiSchu/spectrum-analyzer

-run install

    $ ./spectrum-analyzer/install

- open systemctl config file to change the user name
- replace the correct username in the line ExecStart=/home/<YOUR-USER>/.spectrum-analyzer/bin/iap_spectrum-analyzer_writer.py

    $ nano %HOME/.config/systemd/user/spectrum-analyzer.service


- create a bash alias for command line access

    $ touch .bash_aliases
    $ nano .bash_aliases

- enter the following line to the file and save/exit the file:

    alias spectrum-analyzer='$HOME/.spectrum-analyzer/bin/spectrum-analyzer'

- then source the bashrc file to make the command avail in the terminal

    $ source .bashrc

- use the edit command in the terminal
- change the "Directory" and Device "Address"

    $ spectrum-analyzer edit

[Usage]
Use through commandline interface anywhere in the terminal
Commands always start with "spectrum-analyzer "
In the following list you can find the most crucial:
-"edit" opens the config file in nano
-"start" will start the program. "TimePerHDF" from config determines next available measurement start (60 seconds will result in start at every full minute. Works the same with 1 hour or 1 day)
-"status" shows a status message
-"stop" stops the program. Any .hdf not fully completed wil be unaccessible.
-"scope [<filename>]" will plot the .hdf file from the time specified in <filename> (format: "yyyy-mm-ddThh:mm:ss" do not add the .hdf here, this will happen within the program)
-"scope" will plot the .hdf of the last fully completed hdf cycle -if available-
You will be promted with a commandlist when typing only "spectrum-analyzer" or a wrong command into the Terminal
